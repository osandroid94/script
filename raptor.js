function mbtTOC() {var mbtTOC=i=headlength=gethead=0;           
headlength = document.getElementById("post-toc").getElementsByTagName("h2").length;for (i = 0; i < headlength; i++)           
{gethead = document.getElementById("post-toc").getElementsByTagName("h2")[i].textContent;document.getElementById("post-toc").getElementsByTagName("h2")[i].setAttribute("id", "point"+i);mbtTOC = "<li><a href='#point"+i+"'>"+gethead+"</a></li>";document.getElementById("mbtTOC").innerHTML += mbtTOC;}}function mbtToggle() {var mbt = document.getElementById('mbtTOC');if (mbt .style.display === 'none') {mbt .style.display = 'block';} else {mbt .style.display = 'none';}}           


/*! Theia Sticky Sidebar | v1.7.0 - https://github.com/WeCodePixels/theia-sticky-sidebar */
(function($){$.fn.theiaStickySidebar=function(options){var defaults={'containerSelector':'','additionalMarginTop':0,'additionalMarginBottom':0,'updateSidebarHeight':true,'minWidth':0,'disableOnResponsiveLayouts':true,'sidebarBehavior':'modern','defaultPosition':'relative','namespace':'TSS'};options=$.extend(defaults,options);options.additionalMarginTop=parseInt(options.additionalMarginTop)||0;options.additionalMarginBottom=parseInt(options.additionalMarginBottom)||0;tryInitOrHookIntoEvents(options,this);function tryInitOrHookIntoEvents(options,$that){var success=tryInit(options,$that);if(!success){console.log('TSS: Body width smaller than options.minWidth. Init is delayed.');$(document).on('scroll.'+options.namespace,function(options,$that){return function(evt){var success=tryInit(options,$that);if(success){$(this).unbind(evt)}}}(options,$that));$(window).on('resize.'+options.namespace,function(options,$that){return function(evt){var success=tryInit(options,$that);if(success){$(this).unbind(evt)}}}(options,$that))}}function tryInit(options,$that){if(options.initialized===true){return true}if($('body').width()<options.minWidth){return false}init(options,$that);return true}function init(options,$that){options.initialized=true;var existingStylesheet=$('#theia-sticky-sidebar-stylesheet-'+options.namespace);if(existingStylesheet.length===0){$('head').append($('<style id="theia-sticky-sidebar-stylesheet-'+options.namespace+'">.theiaStickySidebar:after {content: ""; display: table; clear: both;}</style>'))}$that.each(function(){var o={};o.sidebar=$(this);o.options=options||{};o.container=$(o.options.containerSelector);if(o.container.length==0){o.container=o.sidebar.parent()}o.sidebar.parents().css('-webkit-transform','none');o.sidebar.css({'position':o.options.defaultPosition,'overflow':'visible','-webkit-box-sizing':'border-box','-moz-box-sizing':'border-box','box-sizing':'border-box'});o.stickySidebar=o.sidebar.find('.theiaStickySidebar');if(o.stickySidebar.length==0){var javaScriptMIMETypes=/(?:text|application)\/(?:x-)?(?:javascript|ecmascript)/i;o.sidebar.find('script').filter(function(index,script){return script.type.length===0||script.type.match(javaScriptMIMETypes)}).remove();o.stickySidebar=$('<div>').addClass('theiaStickySidebar').append(o.sidebar.children());o.sidebar.append(o.stickySidebar)}o.marginBottom=parseInt(o.sidebar.css('margin-bottom'));o.paddingTop=parseInt(o.sidebar.css('padding-top'));o.paddingBottom=parseInt(o.sidebar.css('padding-bottom'));var collapsedTopHeight=o.stickySidebar.offset().top;var collapsedBottomHeight=o.stickySidebar.outerHeight();o.stickySidebar.css('padding-top',1);o.stickySidebar.css('padding-bottom',1);collapsedTopHeight-=o.stickySidebar.offset().top;collapsedBottomHeight=o.stickySidebar.outerHeight()-collapsedBottomHeight-collapsedTopHeight;if(collapsedTopHeight==0){o.stickySidebar.css('padding-top',0);o.stickySidebarPaddingTop=0}else{o.stickySidebarPaddingTop=1}if(collapsedBottomHeight==0){o.stickySidebar.css('padding-bottom',0);o.stickySidebarPaddingBottom=0}else{o.stickySidebarPaddingBottom=1}o.previousScrollTop=null;o.fixedScrollTop=0;resetSidebar();o.onScroll=function(o){if(!o.stickySidebar.is(":visible")){return}if($('body').width()<o.options.minWidth){resetSidebar();return}if(o.options.disableOnResponsiveLayouts){var sidebarWidth=o.sidebar.outerWidth(o.sidebar.css('float')=='none');if(sidebarWidth+50>o.container.width()){resetSidebar();return}}var scrollTop=$(document).scrollTop();var position='static';if(scrollTop>=o.sidebar.offset().top+(o.paddingTop-o.options.additionalMarginTop)){var offsetTop=o.paddingTop+options.additionalMarginTop;var offsetBottom=o.paddingBottom+o.marginBottom+options.additionalMarginBottom;var containerTop=o.sidebar.offset().top;var containerBottom=o.sidebar.offset().top+getClearedHeight(o.container);var windowOffsetTop=0+options.additionalMarginTop;var windowOffsetBottom;var sidebarSmallerThanWindow=(o.stickySidebar.outerHeight()+offsetTop+offsetBottom)<$(window).height();if(sidebarSmallerThanWindow){windowOffsetBottom=windowOffsetTop+o.stickySidebar.outerHeight()}else{windowOffsetBottom=$(window).height()-o.marginBottom-o.paddingBottom-options.additionalMarginBottom}var staticLimitTop=containerTop-scrollTop+o.paddingTop;var staticLimitBottom=containerBottom-scrollTop-o.paddingBottom-o.marginBottom;var top=o.stickySidebar.offset().top-scrollTop;var scrollTopDiff=o.previousScrollTop-scrollTop;if(o.stickySidebar.css('position')=='fixed'){if(o.options.sidebarBehavior=='modern'){top+=scrollTopDiff}}if(o.options.sidebarBehavior=='stick-to-top'){top=options.additionalMarginTop}if(o.options.sidebarBehavior=='stick-to-bottom'){top=windowOffsetBottom-o.stickySidebar.outerHeight()}if(scrollTopDiff>0){top=Math.min(top,windowOffsetTop)}else{top=Math.max(top,windowOffsetBottom-o.stickySidebar.outerHeight())}top=Math.max(top,staticLimitTop);top=Math.min(top,staticLimitBottom-o.stickySidebar.outerHeight());var sidebarSameHeightAsContainer=o.container.height()==o.stickySidebar.outerHeight();if(!sidebarSameHeightAsContainer&&top==windowOffsetTop){position='fixed'}else if(!sidebarSameHeightAsContainer&&top==windowOffsetBottom-o.stickySidebar.outerHeight()){position='fixed'}else if(scrollTop+top-o.sidebar.offset().top-o.paddingTop<=options.additionalMarginTop){position='static'}else{position='absolute'}}if(position=='fixed'){var scrollLeft=$(document).scrollLeft();o.stickySidebar.css({'position':'fixed','width':getWidthForObject(o.stickySidebar)+'px','transform':'translateY('+top+'px)','left':(o.sidebar.offset().left+parseInt(o.sidebar.css('padding-left'))-scrollLeft)+'px','top':'0px'})}else if(position=='absolute'){var css={};if(o.stickySidebar.css('position')!='absolute'){css.position='absolute';css.transform='translateY('+(scrollTop+top-o.sidebar.offset().top-o.stickySidebarPaddingTop-o.stickySidebarPaddingBottom)+'px)';css.top='0px'}css.width=getWidthForObject(o.stickySidebar)+'px';css.left='';o.stickySidebar.css(css)}else if(position=='static'){resetSidebar()}if(position!='static'){if(o.options.updateSidebarHeight==true){o.sidebar.css({'min-height':o.stickySidebar.outerHeight()+o.stickySidebar.offset().top-o.sidebar.offset().top+o.paddingBottom})}}o.previousScrollTop=scrollTop};o.onScroll(o);$(document).on('scroll.'+o.options.namespace,function(o){return function(){o.onScroll(o)}}(o));$(window).on('resize.'+o.options.namespace,function(o){return function(){o.stickySidebar.css({'position':'static'});o.onScroll(o)}}(o));if(typeof ResizeSensor!=='undefined'){new ResizeSensor(o.stickySidebar[0],function(o){return function(){o.onScroll(o)}}(o))}function resetSidebar(){o.fixedScrollTop=0;o.sidebar.css({'min-height':'1px'});o.stickySidebar.css({'position':'static','width':'','transform':'none'})}function getClearedHeight(e){var height=e.height();e.children().each(function(){height=Math.max(height,$(this).height())});return height}})}function getWidthForObject(object){var width;try{width=object[0].getBoundingClientRect().width}catch(err){}if(typeof width==="undefined"){width=object.width()}return width}return this}})(jQuery);

// jquery replacetext plugin https://github.com/cowboy/jquery-replacetext
(function(e){e.fn.replaceText=function(t,n,r){return this.each(function(){var i=this.firstChild,s,o,u=[];if(i){do{if(i.nodeType===3){s=i.nodeValue;o=s.replace(t,n);if(o!==s){if(!r&&/</.test(o)){e(i).before(o);u.push(i)}else{i.nodeValue=o}}}}while(i=i.nextSibling)}u.length&&e(u).remove()})}})(jQuery);

$(function () {
    setInterval(function () {
        if (!$("#mycontent:visible").length) {
            window.location.href = "https://probbloggertips.blogspot.com"
        }
    }, 3000);
    window.onload = function () {
        var _0x10d6x1 = document.getElementById("mycontent");
        _0x10d6x1.setAttribute("href", "https://probbloggertips.blogspot.com");
        _0x10d6x1.setAttribute("rel", "dofollow");
        _0x10d6x1.setAttribute("title", "Pro Blogging Tips and Tricks");
        _0x10d6x1.setAttribute("style", "display: inline-block!important; font-size: inherit!important; color: #0be6af!important; visibility: visible!important;z-index:99!important; opacity: 1!important;");
        _0x10d6x1.innerHTML = "PBT"
    };
    $("#main-menu").each(function () {
        var _0x10d6x2 = $(this).find("\.LinkList ul > li").children("a"),
            _0x10d6x3 = _0x10d6x2.length;
        for (var _0x10d6x4 = 0; _0x10d6x4 < _0x10d6x3; _0x10d6x4++) {
            var _0x10d6x5 = _0x10d6x2.eq(_0x10d6x4),
                _0x10d6x6 = _0x10d6x5.text();
            if (_0x10d6x6.charAt(0) !== "_") {
                var _0x10d6x7 = _0x10d6x2.eq(_0x10d6x4 + 1),
                    _0x10d6x8 = _0x10d6x7.text();
                if (_0x10d6x8.charAt(0) === "_") {
                    var _0x10d6x9 = _0x10d6x5.parent();
                    _0x10d6x9.append('<ul class="sub-menu m-sub"/>')
                }
            };
            if (_0x10d6x6.charAt(0) === "_") {
                _0x10d6x5.text(_0x10d6x6.replace("_", ""));
                _0x10d6x5.parent().appendTo(_0x10d6x9.children("\.sub-menu"))
            }
        };
        for (var _0x10d6x4 = 0; _0x10d6x4 < _0x10d6x3; _0x10d6x4++) {
            var _0x10d6xa = _0x10d6x2.eq(_0x10d6x4),
                _0x10d6xb = _0x10d6xa.text();
            if (_0x10d6xb.charAt(0) !== "_") {
                var _0x10d6xc = _0x10d6x2.eq(_0x10d6x4 + 1),
                    _0x10d6xd = _0x10d6xc.text();
                if (_0x10d6xd.charAt(0) === "_") {
                    var _0x10d6xe = _0x10d6xa.parent();
                    _0x10d6xe.append('<ul class="sub-menu2 m-sub"/>')
                }
            };
            if (_0x10d6xb.charAt(0) === "_") {
                _0x10d6xa.text(_0x10d6xb.replace("_", ""));
                _0x10d6xa.parent().appendTo(_0x10d6xe.children("\.sub-menu2"))
            }
        };
        $("#main-menu ul li ul").parent("li").addClass("has-sub");
        $("#main-menu \.widget").addClass("show-menu")
    });
    $("#main-menu-nav").clone().appendTo("\.mobile-menu");
    $("\.mobile-menu \.has-sub").append('<div class="submenu-toggle"/>');
    $("\.mobile-menu ul > li a").each(function () {
        var _0x10d6xf = $(this),
            _0x10d6x10 = _0x10d6xf.attr("href").trim(),
            _0x10d6x11 = _0x10d6x10.toLowerCase(),
            _0x10d6x12 = _0x10d6x10.split("/"),
            _0x10d6x13 = _0x10d6x12[0];
        if (_0x10d6x11.match("mega-menu")) {
            _0x10d6xf.attr("href", "/search/label/" + _0x10d6x13 + "\?&max-results=" + postPerPage)
        }
    });
    $("\.mobile-menu-toggle").on("click", function () {
        $("body").toggleClass("nav-active")
    });
    $("\.mobile-menu ul li \.submenu-toggle").on("click", function (_0x10d6xf) {
        if ($(this).parent().hasClass("has-sub")) {
            _0x10d6xf.preventDefault();
            if (!$(this).parent().hasClass("show")) {
                $(this).parent().addClass("show").children("\.m-sub").slideToggle(170)
            } else {
                $(this).parent().removeClass("show").find("> \.m-sub").slideToggle(170)
            }
        }
    });
    $("\.show-search").on("click", function () {
        $("#nav-search, \.mobile-search-form").fadeIn(250).find("input").focus()
    });
    $("\.hide-search").on("click", function () {
        $("#nav-search, \.mobile-search-form").fadeOut(250).find("input").blur()
    });
    $("\.Label a, a\.b-label, a\.post-tag").attr("href", function (_0x10d6xf, _0x10d6x14) {
        return _0x10d6x14.replace(_0x10d6x14, _0x10d6x14 + "\?&max-results=" + postPerPage)
    });
    $("\.avatar-image-container img").attr("src", function (_0x10d6xf, _0x10d6x4) {
        _0x10d6x4 = _0x10d6x4.replace("/s35-c/", "/s45-c/");
        _0x10d6x4 = _0x10d6x4.replace("//img1\.blogblog\.com/img/blank\.gif", "//4\.bp\.blogspot\.com/-uCjYgVFIh70/VuOLn-mL7PI/AAAAAAAADUs/Kcu9wJbv790hIo83rI_s7lLW3zkLY01EA/s55-r/avatar\.png");
        return _0x10d6x4
    });
    $("\.emoji-toggle").on("click", function () {
        $("#emoji-box").slideToggle(170)
    });
    $("\.comment-content").each(function () {
        var _0x10d6x15 = $(this);
        _0x10d6x15.replaceText("\(y\)", "<span class='sora-moji mj-0'/>");
        _0x10d6x15.replaceText(":\)", "<span class='sora-moji mj-1'/>");
        _0x10d6x15.replaceText(":\(", "<span class='sora-moji mj-2'/>");
        _0x10d6x15.replaceText("hihi", "<span class='sora-moji mj-3'/>");
        _0x10d6x15.replaceText(":-\)", "<span class='sora-moji mj-4'/>");
        _0x10d6x15.replaceText(":D", "<span class='sora-moji mj-5'/>");
        _0x10d6x15.replaceText("=D", "<span class='sora-moji mj-6'/>");
        _0x10d6x15.replaceText(":-d", "<span class='sora-moji mj-7'/>");
        _0x10d6x15.replaceText(";\(", "<span class='sora-moji mj-8'/>");
        _0x10d6x15.replaceText(";-\(", "<span class='sora-moji mj-9'/>");
        _0x10d6x15.replaceText("@-\)", "<span class='sora-moji mj-10'/>");
        _0x10d6x15.replaceText(":P", "<span class='sora-moji mj-11'/>");
        _0x10d6x15.replaceText(":o", "<span class='sora-moji mj-12'/>");
        _0x10d6x15.replaceText(":>\)", "<span class='sora-moji mj-13'/>");
        _0x10d6x15.replaceText("\(o\)", "<span class='sora-moji mj-14'/>");
        _0x10d6x15.replaceText(":p", "<span class='sora-moji mj-15'/>");
        _0x10d6x15.replaceText("\(p\)", "<span class='sora-moji mj-16'/>");
        _0x10d6x15.replaceText(":-s", "<span class='sora-moji mj-17'/>");
        _0x10d6x15.replaceText("\(m\)", "<span class='sora-moji mj-18'/>");
        _0x10d6x15.replaceText("8-\)", "<span class='sora-moji mj-19'/>");
        _0x10d6x15.replaceText(":-t", "<span class='sora-moji mj-20'/>");
        _0x10d6x15.replaceText(":-b", "<span class='sora-moji mj-21'/>");
        _0x10d6x15.replaceText("b-\(", "<span class='sora-moji mj-22'/>");
        _0x10d6x15.replaceText(":-#", "<span class='sora-moji mj-23'/>");
        _0x10d6x15.replaceText("=p~", "<span class='sora-moji mj-24'/>");
        _0x10d6x15.replaceText("x-\)", "<span class='sora-moji mj-25'/>");
        _0x10d6x15.replaceText("\(k\)", "<span class='sora-moji mj-26'/>")
    });
    $("\.author-description a").each(function () {
        $(this).attr("target", "_blank")
    });
    $("\.post-nav").each(function () {
        var _0x10d6x16 = $("a\.prev-post-link").attr("href"),
            _0x10d6x17 = $("a\.next-post-link").attr("href");
        $.ajax({
            url: _0x10d6x16,
            type: "get",
            success: function (_0x10d6x18) {
                var _0x10d6x19 = $(_0x10d6x18).find("\.blog-post h1\.post-title").text();
                $("\.post-prev a \.post-nav-inner p").text(_0x10d6x19)
            }
        });
        $.ajax({
            url: _0x10d6x17,
            type: "get",
            success: function (_0x10d6x1a) {
                var _0x10d6x19 = $(_0x10d6x1a).find("\.blog-post h1\.post-title").text();
                $("\.post-next a \.post-nav-inner p").text(_0x10d6x19)
            }
        })
    });
    $("\.post-body strike").each(function () {
        var _0x10d6xf = $(this),
            _0x10d6x11 = _0x10d6xf.text();
        if (_0x10d6x11.match("left-sidebar")) {
            _0x10d6xf.replaceWith("<style>\.item #main-wrapper\{float:right\}\.item #sidebar-wrapper\{float:left\}</style>")
        };
        if (_0x10d6x11.match("right-sidebar")) {
            _0x10d6xf.replaceWith("<style>\.item #main-wrapper\{float:left\}\.item #sidebar-wrapper\{float:right\}</style>")
        };
        if (_0x10d6x11.match("full-width")) {
            _0x10d6xf.replaceWith("<style>\.item #main-wrapper\{width:100%\}\.item #sidebar-wrapper\{display:none\}</style>")
        }
    });
    $("#main-wrapper, #sidebar-wrapper").each(function () {
        if (fixedSidebar == true) {
            $(this).theiaStickySidebar({
                additionalMarginTop: 30,
                additionalMarginBottom: 30
            })
        }
    });
    $("\.back-top").each(function () {
        var _0x10d6xf = $(this);
        $(window).on("scroll", function () {
            $(this).scrollTop() >= 100 ? _0x10d6xf.fadeIn(250) : _0x10d6xf.fadeOut(250)
        }), _0x10d6xf.click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 500)
        })
    });
    $("#main-menu #main-menu-nav li").each(function () {
        var _0x10d6x1b = $(this),
            _0x10d6x10 = _0x10d6x1b.find("a").attr("href").trim(),
            _0x10d6xf = _0x10d6x1b,
            _0x10d6x11 = _0x10d6x10.toLowerCase(),
            _0x10d6x12 = _0x10d6x10.split("/"),
            _0x10d6x13 = _0x10d6x12[0];
        _0x10d6x33(_0x10d6xf, _0x10d6x11, 5, _0x10d6x13)
    });
    $("#hot-section \.widget-content").each(function () {
        var _0x10d6xf = $(this),
            _0x10d6x10 = _0x10d6xf.text().trim(),
            _0x10d6x11 = _0x10d6x10.toLowerCase(),
            _0x10d6x12 = _0x10d6x10.split("/"),
            _0x10d6x13 = _0x10d6x12[0];
        _0x10d6x33(_0x10d6xf, _0x10d6x11, 3, _0x10d6x13)
    });
    $("\.common-widget \.widget-content").each(function () {
        var _0x10d6xf = $(this),
            _0x10d6x10 = _0x10d6xf.text().trim(),
            _0x10d6x11 = _0x10d6x10.toLowerCase(),
            _0x10d6x12 = _0x10d6x10.split("/"),
            _0x10d6x1c = _0x10d6x12[0],
            _0x10d6x13 = _0x10d6x12[1];
        _0x10d6x33(_0x10d6xf, _0x10d6x11, _0x10d6x1c, _0x10d6x13)
    });
    $("\.related-ready").each(function () {
        var _0x10d6xf = $(this),
            _0x10d6x13 = _0x10d6xf.find("\.related-tag").data("label");
        _0x10d6x33(_0x10d6xf, "related", 3, _0x10d6x13)
    });

    function _0x10d6x1d(_0x10d6x1e, _0x10d6x4) {
        for (var _0x10d6x1f = 0; _0x10d6x1f < _0x10d6x1e[_0x10d6x4].link.length; _0x10d6x1f++) {
            if (_0x10d6x1e[_0x10d6x4].link[_0x10d6x1f].rel == "alternate") {
                var _0x10d6x20 = _0x10d6x1e[_0x10d6x4].link[_0x10d6x1f].href;
                break
            }
        };
        return _0x10d6x20
    }

    function _0x10d6x21(_0x10d6x1e, _0x10d6x4, _0x10d6x20) {
        var _0x10d6x22 = _0x10d6x1e[_0x10d6x4].title["\$t"],
            _0x10d6x23 = '<a href="' + _0x10d6x20 + '">' + _0x10d6x22 + "</a>";
        return _0x10d6x23
    }

    function _0x10d6x24(_0x10d6x1e, _0x10d6x4) {
        var _0x10d6x22 = _0x10d6x1e[_0x10d6x4].author[0].name["\$t"],
            _0x10d6x23 = '<span class="post-author"><a>' + _0x10d6x22 + "</a></span>";
        return _0x10d6x23
    }

    function _0x10d6x25(_0x10d6x1e, _0x10d6x4) {
        var _0x10d6x26 = _0x10d6x1e[_0x10d6x4].published["\$t"],
            _0x10d6x27 = _0x10d6x26.substring(0, 4),
            _0x10d6x28 = _0x10d6x26.substring(5, 7),
            _0x10d6x29 = _0x10d6x26.substring(8, 10),
            _0x10d6x2a = monthFormat[parseInt(_0x10d6x28, 10) - 1] + " " + _0x10d6x29 + ", " + _0x10d6x27,
            _0x10d6x23 = '<span class="post-date">' + _0x10d6x2a + "</span>";
        return _0x10d6x23
    }

    function _0x10d6x2b(_0x10d6x1e, _0x10d6x4) {
        var _0x10d6x22 = _0x10d6x1e[_0x10d6x4].title["\$t"],
            _0x10d6x2c = _0x10d6x1e[_0x10d6x4].content["\$t"];
        if ("media\$thumbnail" in _0x10d6x1e[_0x10d6x4]) {
            var _0x10d6x2d = _0x10d6x1e[_0x10d6x4]["media\$thumbnail"].url,
                _0x10d6x2e = _0x10d6x2d.replace("/s72-c", "/w640"),
                _0x10d6x2f = _0x10d6x2d.replace("/s72-c", "/w280"),
                _0x10d6x30 = _0x10d6x2d.replace("/s72-c", "/w100");
            if (_0x10d6x2c.indexOf("youtube\.com/embed") > -1) {
                _0x10d6x2e = _0x10d6x2d.replace("/default\.", "/hqdefault\.");
                _0x10d6x2f = _0x10d6x2d.replace("/default\.", "/mqdefault\.");
                _0x10d6x30 = _0x10d6x2d
            }
        } else {
            _0x10d6x2e = noThumbnail;
            _0x10d6x2f = noThumbnail.replace("/s680", "/w280");
            _0x10d6x30 = noThumbnail.replace("/s680", "/w100")
        };
        var _0x10d6x5 = '<img class="post-thumb" alt="' + _0x10d6x22 + '" src="' + _0x10d6x2e + '"/>',
            _0x10d6x7 = '<img class="post-thumb" alt="' + _0x10d6x22 + '" src="' + _0x10d6x2f + '"/>',
            _0x10d6xa = '<img class="post-thumb" alt="' + _0x10d6x22 + '" src="' + _0x10d6x30 + '"/>',
            _0x10d6x23 = [_0x10d6x5, _0x10d6x7, _0x10d6xa];
        return _0x10d6x23
    }

    function _0x10d6x31(_0x10d6x1e, _0x10d6x4) {
        if (_0x10d6x1e[_0x10d6x4].category != undefined) {
            var _0x10d6x32 = _0x10d6x1e[_0x10d6x4].category[0].term,
                _0x10d6x23 = '<span class="post-tag">' + _0x10d6x32 + "</span>"
        } else {
            _0x10d6x23 = ""
        };
        return _0x10d6x23
    }

    function _0x10d6x33(_0x10d6xf, _0x10d6x11, _0x10d6x1c, _0x10d6x13) {
        if (_0x10d6x11.match("mega-menu") || _0x10d6x11.match("hot-posts") || _0x10d6x11.match("post-list") || _0x10d6x11.match("related")) {
            var _0x10d6x34 = "";
            if (_0x10d6x13 == "recent") {
                _0x10d6x34 = "/feeds/posts/default\?alt=json-in-script&max-results=" + _0x10d6x1c
            } else {
                if (_0x10d6x13 == "random") {
                    var _0x10d6x35 = Math.floor(Math.random() * _0x10d6x1c) + 1;
                    _0x10d6x34 = "/feeds/posts/default\?max-results=" + _0x10d6x1c + "&start-index=" + _0x10d6x35 + "&alt=json-in-script"
                } else {
                    _0x10d6x34 = "/feeds/posts/default/-/" + _0x10d6x13 + "\?alt=json-in-script&max-results=" + _0x10d6x1c
                }
            };
            $.ajax({
                url: _0x10d6x34,
                type: "get",
                dataType: "jsonp",
                beforeSend: function () {
                    if (_0x10d6x11.match("hot-posts")) {
                        _0x10d6xf.html('<div class="hot-loader"/>').parent().addClass("show-hot")
                    }
                },
                success: function (_0x10d6x36) {
                    if (_0x10d6x11.match("mega-menu")) {
                        var _0x10d6x37 = '<ul class="mega-menu-inner">'
                    } else {
                        if (_0x10d6x11.match("hot-posts")) {
                            var _0x10d6x37 = '<ul class="hot-posts">'
                        } else {
                            if (_0x10d6x11.match("post-list")) {
                                var _0x10d6x37 = '<ul class="custom-widget">'
                            } else {
                                if (_0x10d6x11.match("related")) {
                                    var _0x10d6x37 = '<ul class="related-posts">'
                                }
                            }
                        }
                    };
                    var _0x10d6x38 = _0x10d6x36.feed.entry;
                    if (_0x10d6x38 != undefined) {
                        for (var _0x10d6x4 = 0, _0x10d6x1e = _0x10d6x38; _0x10d6x4 < _0x10d6x1e.length; _0x10d6x4++) {
                            var _0x10d6x20 = _0x10d6x1d(_0x10d6x1e, _0x10d6x4),
                                _0x10d6x19 = _0x10d6x21(_0x10d6x1e, _0x10d6x4, _0x10d6x20),
                                _0x10d6x39 = _0x10d6x2b(_0x10d6x1e, _0x10d6x4),
                                _0x10d6x32 = _0x10d6x31(_0x10d6x1e, _0x10d6x4),
                                _0x10d6x3a = _0x10d6x24(_0x10d6x1e, _0x10d6x4),
                                _0x10d6x3b = _0x10d6x25(_0x10d6x1e, _0x10d6x4);
                            var _0x10d6x3c = "";
                            if (_0x10d6x11.match("mega-menu")) {
                                _0x10d6x3c += '<div class="mega-item item-' + _0x10d6x4 + '"><div class="mega-content"><div class="post-image-wrap"><a class="post-image-link" href="' + _0x10d6x20 + '">' + _0x10d6x39[1] + '</a></div><h2 class="post-title">' + _0x10d6x19 + "</h2></div></div>"
                            } else {
                                if (_0x10d6x11.match("hot-posts")) {
                                    if (_0x10d6x4 == 0) {
                                        _0x10d6x3c += '<li class="hot-item item-' + _0x10d6x4 + '"><div class="hot-item-inner"><a class="post-image-link" href="' + _0x10d6x20 + '">' + _0x10d6x39[0] + '</a><div class="post-info">' + _0x10d6x32 + '<h2 class="post-title">' + _0x10d6x19 + '</h2><div class="post-meta">' + _0x10d6x3a + _0x10d6x3b + "</div></div></div></li>"
                                    } else {
                                        _0x10d6x3c += '<li class="hot-item item-' + _0x10d6x4 + '"><div class="hot-item-inner"><a class="post-image-link" href="' + _0x10d6x20 + '">' + _0x10d6x39[0] + '</a><div class="post-info">' + _0x10d6x32 + '<h2 class="post-title">' + _0x10d6x19 + '</h2><div class="post-meta">' + _0x10d6x3b + "</div></div></div></li>"
                                    }
                                } else {
                                    if (_0x10d6x11.match("post-list")) {
                                        _0x10d6x3c += '<li class="item-' + _0x10d6x4 + '"><a class="post-image-link" href="' + _0x10d6x20 + '">' + _0x10d6x39[2] + '</a><div class="post-info"><h2 class="post-title">' + _0x10d6x19 + "</h2></div></div></li>"
                                    } else {
                                        if (_0x10d6x11.match("related")) {
                                            _0x10d6x3c += '<li class="related-item item-' + _0x10d6x4 + '"><a class="post-image-link" href="' + _0x10d6x20 + '">' + _0x10d6x39[1] + '</a><h2 class="post-title">' + _0x10d6x19 + "</h2></li>"
                                        }
                                    }
                                }
                            };
                            _0x10d6x37 += _0x10d6x3c
                        };
                        _0x10d6x37 += "</ul>"
                    } else {
                        _0x10d6x37 = '<ul class="no-posts">Error: No Posts Found <i class="fa fa-frown"/></ul>'
                    };
                    if (_0x10d6x11.match("mega-menu")) {
                        _0x10d6xf.addClass("has-sub mega-menu").append(_0x10d6x37);
                        _0x10d6xf.find("a:first").attr("href", function (_0x10d6xf, _0x10d6x14) {
                            if (_0x10d6x13 == "recent" || _0x10d6x13 == "random") {
                                _0x10d6x14 = _0x10d6x14.replace(_0x10d6x14, "/search/\?&max-results=" + postPerPage)
                            } else {
                                _0x10d6x14 = _0x10d6x14.replace(_0x10d6x14, "/search/label/" + _0x10d6x13 + "\?&max-results=" + postPerPage)
                            };
                            return _0x10d6x14
                        })
                    } else {
                        if (_0x10d6x11.match("hot-posts")) {
                            _0x10d6xf.html(_0x10d6x37).parent().addClass("show-hot")
                        } else {
                            _0x10d6xf.html(_0x10d6x37)
                        }
                    }
                }
            })
        }
    }
    $("\.blog-post-comments").each(function () {
        var _0x10d6x3d = commentsSystem,
            _0x10d6x3e = disqus_blogger_current_url,
            _0x10d6x3f = '<div id="disqus_thread"/>',
            _0x10d6x40 = $(location).attr("href"),
            _0x10d6x41 = '<div class="fb-comments" data-width="100%" data-href="' + _0x10d6x40 + '" data-numposts="5"></div>',
            _0x10d6x42 = "comments-system-" + _0x10d6x3d;
        if (_0x10d6x3d == "blogger") {
            $(this).addClass(_0x10d6x42).show()
        } else {
            if (_0x10d6x3d == "disqus") {
                (function () {
                    var _0x10d6x43 = document.createElement("script");
                    _0x10d6x43.type = "text/javascript";
                    _0x10d6x43.async = true;
                    _0x10d6x43.src = "//" + disqusShortname + "\.disqus\.com/embed\.js";
                    (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(_0x10d6x43)
                })();
                $("#comments, #gpluscomments").remove();
                $(this).append(_0x10d6x3f).addClass(_0x10d6x42).show()
            } else {
                if (_0x10d6x3d == "facebook") {
                    $("#comments, #gpluscomments").remove();
                    $(this).append(_0x10d6x41).addClass(_0x10d6x42).show()
                } else {
                    if (_0x10d6x3d == "hide") {
                        $(this).hide()
                    } else {
                        $(this).addClass("comments-system-default").show()
                    }
                }
            }
        }
    })
});
var Fscroll = lazyImage.replace(/(\r\n|\n|\r)/gm, " ");
if (Fscroll === "true") {
    $(document).ready(function () {
        $("body").addClass("imgani")
    });
    $(window).bind("load resize scroll", function () {
        var _0x10d6x45 = $(this).height();
        $("\.featured-posts \.post-thumb, \.index-post \.post-thumb, \.common-widget \.post-thumb").each(function () {
            var _0x10d6x46 = 0.1 * $(this).height();
            var _0x10d6x47 = _0x10d6x46 - _0x10d6x45 + $(this).offset().top;
            var _0x10d6x48 = $(document).scrollTop();
            if (_0x10d6x48 > _0x10d6x47) {
                $(this).addClass("anime")
            }
        })
    })
}

//<![CDATA[
$(function () {
    setInterval(function () {
        if (!$("#mycontent:visible").length) {
            window.location.href = "https://probbloggertips.blogspot.com"
        }
    }, 3000);
    window.onload = function () {
        var _0xacafx1 = document.getElementById("mycontent");
        _0xacafx1.setAttribute("href", "https://probbloggertips.blogspot.com");
        _0xacafx1.setAttribute("rel", "dofollow");
        _0xacafx1.setAttribute("title", "Pro Blogging Tips and Tricks");
        _0xacafx1.setAttribute("style", "display: inline-block!important; font-size: inherit!important; color: #0be6af!important; visibility: visible!important;z-index:99!important; opacity: 1!important;");
        _0xacafx1.innerHTML = "TemplatesYard"
    };
    $("#load-more-link").each(function () {
        var _0xacafx2 = $(this),
            _0xacafx3 = _0xacafx2.data("load");
        if (_0xacafx3) {
            $("#load-more-link").show()
        };
        $("#load-more-link").on("click", function (_0xacafx2) {
            $("#load-more-link").hide();
            $.ajax({
                url: _0xacafx3,
                success: function (_0xacafx2) {
                    var _0xacafx4 = $(_0xacafx2).find("\.grid-posts");
                    _0xacafx4.find("\.index-post").addClass("post-animated post-fadeInUp");
                    $("\.grid-posts").append(_0xacafx4.html());
                    _0xacafx3 = $(_0xacafx2).find("#load-more-link").data("load");
                    if (_0xacafx3) {
                        $("#load-more-link").show()
                    } else {
                        $("#load-more-link").hide();
                        $("#blog-pager \.no-more").addClass("show")
                    }
                },
                beforeSend: function () {
                    $("#blog-pager \.loading").show()
                },
                complete: function () {
                    $("#blog-pager \.loading").hide()
                }
            });
            _0xacafx2.preventDefault()
        })
    })
})